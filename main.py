import requests
from fake_useragent import UserAgent
from bs4 import BeautifulSoup
import lxml
import csv
import json

url = 'https://www.kinopoisk.ru/lists/movies/top_1000/?page={}'

ua = UserAgent()

session = requests.Session()

session.headers = {
    'UserAgent': ua.random
}

final_data = []
film_number = iter(range(1, 1000))

session.headers.update({
    "accept": "*/*",
    "accept-language": "ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7",
    "accept-encoding": "gzip, deflate, br",
    "sec-fetch-dest": "document", "sec-fetch-mode": "navigate",
    "sec-fetch-site": "same-origin",
    "upgrade-insecure-requests": "1",
    "cookie": "_csrf=80_LpML5qIoacUtcplw-uCXM; yandex_login=igorian401; i=tnisIFFWdTx7K4qFrKtTUcSbw6kE67axi8GjoSuIg0jtJlHHaHvU6l8oxitGZAkU+Oa1vQlgiZiXsg51pJybg/UCt90=; yandexuid=1165665981630397032; L=eElhCV5yAAN2SGB+Un15cnFkDQJ5cHVnWAY2HiMYJ2VgQQ==.1693802955.15455.31551.6ac6759a9398ea53f2427af9d4109c1f; desktop_session_key=d39438b85d5978dce626201a0eaff14db6711c9f01b0103dea93e99af5e6085b0c6d2dea823c3d810d0c17511dc2e71942692532dd42279b1d042c69051ff499acceefde74a2cb6b30a4c343e98422f36346388c89c8239e525f5426340198f2; desktop_session_key.sig=23Ml7JKcUHbgxwE_Q6gls6moqks; gdpr=0; _ym_uid=167999560592185994; _ym_isad=2; SLG_G_WPT_TO=ru; yuidss=1165665981630397032; yp=1695901328.yu.1165665981630397032; ymex=1698406928.oyu.1165665981630397032; SLG_GWPT_Show_Hide_tmp=1; SLG_wptGlobTipTmp=1; location=1; crookie=5UkyD35lFgd6poZm5ijR6Me4tdc7djyiMYP4bhbWpj68/uoZkAfVZRMLAy0x0vohfaB688RDfUnhxBNhXADrvPn5jyM=; cmtchd=MTY5NTgxOTMxNjkwNw==; coockoos=7; _yasc=ficHPeZLcOb8E01UxCkvgGcddoxRiTLUfTDyr0NRWfgKrpKpvUr3RNk8YTNkGSXe; ya_sess_id=3:1695885343.5.0.1693802955894:nLjovA:7.1.2:1|587431304.-1.2.3:1693802955|30:10219246.489181.dzdtDaEGyxjRxslm23ILFT7xHAM; sessar=1.1182.CiB4MF3eJOi6Te21XZVQs0r1fcyy-rlaRp3bPgGLC1RY2Q.Bfu6V6flIuSwjvkdvOc_XB1OEjwtp5yRzprI62WK3q4; ys=udn.cDppZ29yaWFuNDAx#c_chck.1783387212; mda2_beacon=1695885343670; sso_status=sso.passport.yandex.ru:synchronized; _ym_d=1695885420"
})

for page_number in range(1, 21):
    print(url.format(page_number))
    resp = session.get(url.format(page_number))
    bs = BeautifulSoup(resp.text, 'lxml')
    s_data = bs.find('script', id='__NEXT_DATA__').contents[0]
    data = json.loads(s_data)

    films_info = data['props']['apolloState']['data']

    films_keys = []

    for i in films_info:
        if 'Film' in i:
            films_keys.append(i)

    for film in films_keys:
        item = {
            'id': next(film_number),
            'film_title': films_info[film]['title']['russian'],
            'product_year': films_info[film]['productionYear'],
            'country': films_info[film]['countries'][0]['name'],
            'director': films_info[film]['members({"limit":1,"role":["DIRECTOR"]})']['items'][0]['person']['name'],
            'rating': films_info[film]['rating']['kinopoisk']['value'],
            'trailer': False if films_info[film]['mainTrailer'] is None else True,
        }
        final_data.append(item)

with open('data.csv', 'w') as file:
    csv_data = csv.writer(file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
    csv_data.writerow(['Id', 'Название', 'Год выпуска', 'Страна', 'Режисер', 'Рейтинг', 'Трейлер'])
    for f in final_data:
        csv_data.writerow(f.values())
